CXX = g++
INC = /home/alan_shrestha/pari_ws/src/mapf/include
CXXFLAGS = -Wall -g -I$(INC) 

graph.o: include/graph.h
	$(CXX) $(CXXFLAGS) -c src/graph.cpp include/graph.h

astar.o: src/astar.cpp include/graph.h include/astar.h include/cbs.h include/lowlevel.h
	$(CXX) $(CXXFLAGS) -c src/astar.cpp include/graph.h include/astar.h include/lowlevel.h include/cbs.h

lowlevel.o: src/lowlevel.cpp include/lowlevel.h include/graph.h include/astar.h include/cbs.h
	$(CXX) $(CXXFLAGS) -c src/lowlevel.cpp include/lowlevel.h include/graph.h include/astar.h include/cbs.h

cbs.o: src/cbs.cpp include/cbs.h include/lowlevel.h
	$(CXX) $(CXXFLAGS) -c src/cbs.cpp include/lowlevel.h include/cbs.h

main: src/main.cpp astar.o graph.o cbs.o lowlevel.o
	$(CXX) $(CXXFLAGS) -o main src/main.cpp astar.o graph.o cbs.o lowlevel.o

all: graph.o astar.o lowlevel.o cbs.o main