#include "gLoader.h"

std::vector<std::pair<float, float>> CSVReader::getData(std::string filename, std::string delim)
{
	std::ifstream file(filename);
	std::vector<std::pair<float, float>> dataList;

	std::string line = "";
	int counter = 0;
	// Iterate through each line and split the content using delimeter
	while (getline(file, line))
	{
		if (counter != 0)
		{
			std::pair<float, float> temppair;
			std::vector<std::string> vec;
			boost::algorithm::split(vec, line, boost::is_any_of(delim));
			temppair.first = stof(vec[0]);
			temppair.second = stof(vec[1]);
			dataList.push_back(temppair);
			//std::cout << temppair.first << "," << temppair.second << std::endl;
		}
		counter++;
	}
	// Close the File
	file.close();

	return dataList;
}

YAMLReader::YAMLReader(std::string mapfolder, std::string filename)
{
	fileName = filename;
	delimeter = ":";
	mapFolder = mapfolder;

	std::vector<std::pair<std::string, std::string>> tempdata;
	std::vector<std::pair<std::string, std::string>> tempdata1;
	tempdata = this->getData(mapFolder + fileName);

	for (std::pair<std::string, std::string> pairdata : tempdata)
	{
		if (pairdata.first == "map")
		{
			mapFile = pairdata.second;
			mapFile.erase(0, 1);
		}
		if (pairdata.first == "node")
		{
			nodeFile = pairdata.second;
			nodeFile.erase(0, 1);
		}
		if (pairdata.first == "edge")
		{
			edgeFile = pairdata.second;
			edgeFile.erase(0, 1);
		}
	}
	//std::cout << mapFile << nodeFile << edgeFile << std::endl;
	//std::cout << mapfolder + mapFile<<std::endl;
	tempdata1 = this->getData(mapFolder + mapFile);
	for (std::pair<std::string, std::string> pairdata : tempdata1)
	{
		if (pairdata.first == "resolution")
		{
			res = stof(pairdata.second);
		}
		if (pairdata.first == "origin")
		{
			std::string temp_origin = pairdata.second;
			//std::cout<<temp_origin<<std::endl;
			std::vector<std::string> vec;
			boost::algorithm::split(vec, temp_origin, boost::is_any_of(","));
			std::string tempo = vec[0];
			tempo.erase(1, 1);
			//std::cout<<vec[0];
			//std::cout<<tempo;
			centerX = stof(tempo);
			centerY = stof(vec[1]);
			/* for(int i =0; i < 2;i++)
				{
					//std::cout<<vec[i]<<std::endl;
			 	}*/
		}
	}
	//std::cout<<res<<centerX<<centerY<<std::endl;
}

std::vector<std::pair<std::string, std::string>> YAMLReader::getData(std::string yamlfile)
{
	std::ifstream file(yamlfile);
	std::vector<std::pair<std::string, std::string>> dataList;

	std::string line = "";
	// Iterate through each line and split the content using delimeter
	while (getline(file, line))
	{
		std::vector<std::string> vec;
		std::pair<std::string, std::string> pair;
		boost::algorithm::split(vec, line, boost::is_any_of(delimeter));
		pair.first = vec[0];
		pair.second = vec[1];
		dataList.push_back(pair);
	}
	// Close the File
	file.close();

	return dataList;
}

int no_main()
{
	// Creating an object of CSVWriter
	//CSVReader reader("../map/points_data.txt");
	std::string map_folder = "../map/";
	std::string mapf_data = "mapf-data.yaml";
	YAMLReader reader(map_folder, mapf_data);

	std::vector<std::pair<float, float>> nodes = CSVReader::getData(reader.mapFolder + reader.nodeFile);
	std::vector<std::pair<float, float>> edges = CSVReader::getData(reader.mapFolder + reader.edgeFile);
	// Get the data from CSV File
	//std::vector<std::vector<std::string>> dataList = reader.getData();

	// Print the content of row by row on screen
	/* for (int i = 0; i < int(dataList.size()); i++)
	{
		if (i == 0)
		{
			std::cout << "Printing node data" << std::endl;
		}
		else
		{
			for (std::string data : dataList[i])
			{
				std::cout << data << " , ";
			}
			std::cout << std::endl;
		}
	} */
	return 0;
}