#include "cbs.h"
#include "lowlevel.h"

using namespace std;

Constraint::Constraint()
{
}

Constraint::Constraint(Agent *agent, Node *node, int timeStep)
{
    this->agent = agent;
    this->node = node;
    this->timeStep = timeStep;
}

ostream & operator << (ostream &out, const Constraint &c)
{
    cout<<"{Agent: "<<*c.agent<<" , Node: "<<*c.node<<" , TimeStep: "<<c.timeStep<< "}";
}

EdgeConstraint::EdgeConstraint()
{
}

EdgeConstraint::EdgeConstraint(Agent* agent, Node* srcNode, Node* destNode, int srcTimeStep)
{
    this->agent = agent;
    this->srcNode = srcNode;
    this->destNode = destNode;
    this->srcTimeStep = srcTimeStep;
}

ostream & operator << (ostream &out, const EdgeConstraint &e)
{
    cout<<"{Agent: "<<*e.agent<<" , srcNode: "<<*e.srcNode<<" , destNode: "<<*e.destNode<<" , TimeStep: "<<e.srcTimeStep<< "}";
}

Agent::Agent(int id, Node *dockStation, int status /*= 0*/)
{
    this->id = id;
    this->dockStation = dockStation;
    this->status = status;
}

ostream & operator << (ostream &out, const Agent &a)
{
    cout<<"{id: "<<a.id<<" , StartNode: "<<*a.startNode<<" , GoalNode: "<<*a.goalNode<< "}";
}

Agent::Agent()
{
}

float CTNode::find_cost()
{
    int sic = 0;
    for (int i=0;i<this->solution.size();i++)
    {
        sic += this->solution[i].second.size();
    }
    return sic;
}

Path CTNode::low_level(Agent *agent, Graph *graph, vector<Constraint> &constraints, vector<EdgeConstraint> &edgeConstraints)
{
    Path path;
    vector<Constraint> relevantConstraints;
    vector<EdgeConstraint> relevantEdgeConstraints;
    for (int i=0;i<constraints.size();i++)
    {
        if(constraints[i].agent == agent)
        {
            relevantConstraints.push_back(constraints[i]);
        }
    }
    for (int i=0; i<edgeConstraints.size(); i++)
    {
        if(edgeConstraints[i].agent == agent)
        {
            relevantEdgeConstraints.push_back(edgeConstraints[i]);
        }
    }

    LowLevel lowLevel;
    lowLevel.graph = graph;
    path = lowLevel.findPath(agent->startNode, agent->goalNode, relevantConstraints, relevantEdgeConstraints);
    return path;
}


Solution solve_cbs(MAPF *mapf)
{
    Graph* graph = mapf->graph;
    vector<Agent*> agents = mapf->agents;

    vector<CTNode> OPEN;
    CTNode R;

    vector<pair<Agent *, Path> > soln;

    for (int i=0;i<agents.size();i++)
    {
        Agent* agent = agents[i];
        pair<Agent *, Path> temppair;
        Path path = R.low_level(agent, graph, R.constraints, R.edgeConstraints);
        temppair.first = agent;
        temppair.second = path;
        soln.push_back(temppair);
    }
    adjust_solution_length(soln);
    R.solution = soln;
    R.cost = R.find_cost();

    OPEN.push_back(R);

    int min_pos = 0;
    
    while (!OPEN.empty())
    {
        //sort and find min_pos
        int node_size = OPEN.size();
        //vector<CTNode> *openptr = &OPEN;
        CTNode P = smallest_cost(OPEN, node_size);

        //CTNode P = OPEN[min_pos];

        Conflict C = P.find_conflict();
        EdgeConflict E = P.find_edgeConflict();

        /// Validate and find if there is conflict or not this is the
        if (C.agents.empty() && E.srcAgent == NULL)
        {
            return P.solution;
        }

        /// If there are any vertex conflicts
        if (!C.agents.empty())
        {
            for (int i=0; i<C.agents.size(); i++)
            {
                Agent* agent = C.agents[i];
                Constraint newconstraint(agent, C.node, C.timeStep);
                CTNode A;
                A.constraints = P.constraints;
                A.constraints.push_back(newconstraint);
                A.solution = P.solution;

                Path temppath = A.low_level(agent, graph, A.constraints, A.edgeConstraints);
                int agent_count = 0;

                //for (pair<Agent *, Path> temppair : A.solution)
                for (int j=0; j<A.solution.size(); j++)
                {
                    //pair<Agent*, Path> temppair = A.solution[j];
                    if (A.solution[j].first == agent)
                    {
                        A.solution[j].second = temppath;
                    }
                }

                adjust_solution_length(A.solution);
                A.cost = A.find_cost();
                OPEN.push_back(A);
            }
        }
        /// Else if there are any Edge Conflicts
        else if (E.srcAgent != NULL)
        {
            /// Unlike vertex conflict, edge conflict cannot be looped over
            /// In vertex conflict, there is a vector of agents occupying the same node that can be looped over
            /// But in edge conflict, there are always two agents with src and dest node

            /// For the first agent
            {
                Agent* srcAgent = E.srcAgent;
                EdgeConstraint newConstraint(srcAgent, E.srcNode, E.destNode, E.srcTimeStep);
                CTNode A;
                A.constraints = P.constraints;
                A.edgeConstraints = P.edgeConstraints;
                A.edgeConstraints.push_back(newConstraint);
                A.solution = P.solution;

                Path temppath = A.low_level(srcAgent, graph, A.constraints, A.edgeConstraints);
                int agent_count = 0;

                for (int j=0; j<A.solution.size(); j++)
                {
                    if (A.solution[j].first == srcAgent)
                    {
                        A.solution[j].second = temppath;
                    }
                }
                adjust_solution_length(A.solution);
                A.cost = A.find_cost();
                OPEN.push_back(A);
            }

            /// For the second agent
            {
                Agent* destAgent = E.destAgent;
                EdgeConstraint newConstraint(destAgent, E.destNode, E.srcNode, E.srcTimeStep);
                CTNode A;
                A.constraints = P.constraints;
                A.edgeConstraints = P.edgeConstraints;
                A.edgeConstraints.push_back(newConstraint);
                A.solution = P.solution;

                Path temppath = A.low_level(destAgent, graph, A.constraints, A.edgeConstraints);
                int agent_count = 0;

                for (int j=0; j<A.solution.size(); j++)
                {
                    if (A.solution[j].first == destAgent)
                    {
                        A.solution[j].second = temppath;
                    }
                }
                adjust_solution_length(A.solution);
                A.cost = A.find_cost();
                OPEN.push_back(A);
            }
        }

    }
    Solution solt;
    return solt;
}

CTNode smallest_cost(vector<CTNode> &ctnodes, int node_size)
{
    vector <CTNode> CTNodes = ctnodes;
    int index = 0;
    for (int i = 0; i < node_size; i++)
    {
        if (CTNodes[i].cost < CTNodes[index].cost)
        {
            index = i;
        }
    }
    CTNode ctNodeIns = CTNodes[index];
    vector<CTNode>::iterator itr = ctnodes.begin() + index;
    ctnodes.erase(itr);
    return ctNodeIns;
}

EdgeConflict CTNode::find_edgeConflict()
{
    vector<Node*> previousNodes;
    int num_agents = solution.size();
    int length_path = solution[0].second.size();
    for (int i=0; i<num_agents; i++)
    {
        previousNodes.push_back(solution[i].second[0]);
    }
    /// For each item in path
    for (int i=1; i<length_path; i++)
    {
        vector<Node*> presentNodes;
        /// For each agent
        for (int j=0; j<num_agents; j++)
        {

            /// Present node of jth agent
            Node* presentNodeJ = solution[j].second[i];
            for (int k=0; k<num_agents; k++)
            {

                /// Present node of the kth agent
                Node* presentNodeK = solution[k].second[i];
                if ( k!=j && presentNodeJ == previousNodes[k] && presentNodeK == previousNodes[j])
                {
                    EdgeConflict C;
                    C.srcAgent = solution[k].first;
                    C.destAgent = solution[j].first;
                    C.srcNode = previousNodes[k];
                    C.destNode = presentNodeK;
                    C.srcTimeStep = i-1;
                    return C;
                }
            }
            /// Push the i-th element
            presentNodes.push_back(presentNodeJ);
        }
        previousNodes = presentNodes;
    }
    EdgeConflict C;
    C.srcAgent = NULL;
    return C;
}

Conflict CTNode::find_conflict()
{
    /// Loop through the corresponding path item for each agent
    for (int i=0; i<this->solution[0].second.size(); i++)
    {
        vector<Node*> nodes;
        /// Make a vector of all the nodes in the i-th timestep of each agent's path
        for (int j=0; j<this->solution.size(); j++)
        {
            nodes.push_back(solution[j].second[i]);
        }
        /// Search througuh the repeated nodes in the vector to find out which agents collide in the given timestep i
        /// Finds the first conflicting node
        pair<Node*, vector<int> > conflict_ids = this->find_repetition(nodes);


        if (!conflict_ids.second.empty())
        {
            Conflict C;
            C.node = conflict_ids.first;
            vector<Agent*> agents;
            for (int k = 0; k<conflict_ids.second.size(); k++)
            {
                Agent* agent = this->solution[conflict_ids.second[k]].first;
                C.agents.push_back(agent);
            }
            C.timeStep = i;
            return C;
        }
    }
    Conflict Con;
    return Con;
}

pair<Node*, vector<int> > CTNode::find_repetition(vector<Node*> &nodes)
{
    for (int i=0; i<nodes.size(); i++)
    {
        vector<int> ids;
        ids.push_back(i);
        Node* node = nodes[i];
        for (int j= i+1; j<nodes.size(); j++)
        {
            if (nodes[i] == nodes[j])
            {
                ids.push_back(j);
            }
            if (ids.size()>1)
            {
                return pair<Node*, vector<int> >(node, ids);
            }
        }
    }
    pair<Node*, vector<int> > temp;
    return temp;
}

void print_solution(Solution &solution)
{
    for (int i=0;i<solution.size();i++)
    {
        cout<<endl<<"-------"<<"Agent "<<solution[i].first->id<<"--------"<<endl;
        for (int j=0;j<solution[i].second.size();j++)
        {
            cout<<*solution[i].second[j];
        }
    }
}

void adjust_solution_length(Solution &solution)
{
    int max_size = 0;
    for (int i=0;i<solution.size();i++)
    {
        int length = solution[i].second.size();
        max_size = (length > max_size)? length: max_size;
    }
    for (int i=0; i<solution.size(); i++)
    {
        while (solution[i].second.size()!=max_size)
        {
            solution[i].second.push_back(solution[i].second[solution[i].second.size()-1]);
        }
    }
}
