#include "lowlevel.h"
#include "graph.h"
#include "astar.h"
#include "cbs.h"
#include<algorithm>

using namespace std;

vector<Node*> LowLevel::findPath(Node* startNode, Node* goalNode, const vector<Constraint> &constraints, const vector<EdgeConstraint> &edgeConstraints)
{
    /// vector of PathNode 's used to reconstruct consistent path
    vector<PathNode> path_constructor;

    this->startNode = startNode;
    this->goalNode = goalNode;

    QElement start;
    vector<Node*> path;
    start.node = startNode;
    start.gValue = 0;

    OPEN.push_back(start);
    startNode->timeStep = 0;
    PathNode pathElement(startNode, 0, -1);
    path_constructor.push_back(pathElement);

    int goalTimeStep = 0;

    /// if there is a constraint such that that its node == goal node store its time step
    /// so that it can be checked against the current time step to know if the there is a collision
    /// after termination , i.e. the agent is resting indefinitely at its goal state.
    this->latestGoalConstraint = 0;

    /// Assign the latest timestep associated with constraint on the goal node (if any) to latestGoalConstraint variable
    /// For example Contraint(agent=someAgent, node=goalNode, timeStep=someTimeStep) means that the someAgent cannot be at goalNode at someTimeStep
    for (int i=0; i<constraints.size(); i++)
    {
        if (constraints[i].node == goalNode)
        {
            if(latestGoalConstraint<constraints[i].timeStep)
                this->latestGoalConstraint = constraints[i].timeStep;
        }
    }


    for(int j=0;;j++)
    {
        /// Fetch the lowest rank element from OPEN and remove it from OPEN
        QElement current;
        lowestRankOpen(current);
        removeNode(OPEN, current.node);
        Node* currentNode = current.node;

        /// expand only if the current node is not goal node or there are no constraints in goal node later than the current node timestep.
        if (!((current.node == goalNode) && (current.node->timeStep > this->latestGoalConstraint)))
        {
            /// Fetch the neighbouring nodes of currentNode from the graph
            vector<Pair> neighbours = graph->returnNeighbours(*currentNode);

            /// Filter out the inconsistent neighbouring nodes given edgeConstraints
            filter_nodes(neighbours, current.node, edgeConstraints);

            /// Track the timeStep for the given Node
            int currentTimeStep = currentNode->timeStep;

            /// Go through all the neigbouring nodes
            for (int i=0; i<neighbours.size(); i++)
            {
                /// TODO: remove the in_closed
                /// Flags to find out if the neighbouring node is in open / closed queue
                /// This is pointless in lowlevel search because
                /// Unlike regular Astar the agent Has to search through visited nodes (path can consist of same nodes at different timesteps)
                bool in_open = false;
                bool in_closed = false;

                /// Compute cost to the current neighbouring node
                float cost = current.gValue + neighbours[i].second;

                /// If neighbor in OPEN and cost less than g(neighbor):
                /// Remove neighbor from OPEN, because new path is better
                float gNeighbourOpen = gValueOpen(neighbours[i].first);
                if (gNeighbourOpen >= 0)
                {
                    in_open = true;
                    if (cost < gNeighbourOpen)
                    {
                        removeNode(OPEN, neighbours[i].first);
                        in_open = false;
                    }
                }

                /// If neighbor in CLOSED and cost less than g(neighbor): ⁽²⁾
                /// Remove neighbor from CLOSED
                float gNeighbourClosed = gValueClosed(neighbours[i].first);
                if (gNeighbourClosed >=0)
                {
                    in_closed = true;
                    if (cost < gNeighbourClosed)
                    {
                        removeNode(CLOSED, neighbours[i].first);
                        in_closed = false;
                    }
                }

                if ((!in_closed) && (!in_open))
                {
                    neighbours[i].first->timeStep = currentTimeStep +1;

                    if (isAllowed(neighbours[i].first, constraints))
                    {
                        PathNode childNode(neighbours[i].first, neighbours[i].first->timeStep, currentNode->id);
                        path_constructor.push_back(childNode);

                        QElement temporary;
                        temporary.node = neighbours[i].first;
                        temporary.gValue = cost;
                        temporary.fValue = temporary.gValue + H(temporary.node);
                        OPEN.push_back(temporary);
                    }
                }
            }
        }
        else
        {
            goalTimeStep = current.node->timeStep;
            break;
        }
    }
    path = reconstructPath(path_constructor, goalTimeStep);
    reverse(path.begin(), path.end());
    return path;
}

void LowLevel::filter_nodes(vector<Pair> &neighbours, Node* currentNode, const vector<EdgeConstraint> &edgeConstraints)
{
    for (int i=0; i<edgeConstraints.size(); i++)
    {
        if (currentNode->timeStep == edgeConstraints[i].srcTimeStep)
        {
            for (int j=0; j<neighbours.size(); j++)
            {
                if (neighbours[j].first == edgeConstraints[i].destNode && currentNode == edgeConstraints[i].srcNode)
                {
                    vector<Pair>::iterator itr = neighbours.begin() + j;
                    neighbours.erase(itr);
                }
            }
        }
    }
}

bool LowLevel::isAllowed(Node* node, const vector<Constraint> &constraints)
{
    for (int i=0;i<constraints.size();i++)
    {
        if (constraints[i].node == node && constraints[i].timeStep == node->timeStep)
        {
            return false;
        }
    }
    return true;
}

vector<Node*> LowLevel::reconstructPath(vector<PathNode> &pathConstructor, int goalTimeStep)
{
    vector<Node*> path;
    int goalLocation = 0;
    for (int i=0;i<pathConstructor.size(); i++)
    {
        if (pathConstructor[i].node == goalNode && pathConstructor[i].timeStep == goalTimeStep)
        {
            goalLocation = i;
            break;
        }
    }
    path.push_back(pathConstructor[goalLocation].node);
    int prev_id = pathConstructor[goalLocation].prev_id;
    int prevTimeStep = pathConstructor[goalLocation].timeStep - 1;
    for (int i=goalLocation;i>=0;i--)
    {
        if (pathConstructor[i].node->id == prev_id && pathConstructor[i].timeStep == prevTimeStep)
        {
            path.push_back(pathConstructor[i].node);
            prev_id = pathConstructor[i].prev_id;
            prevTimeStep = pathConstructor[i].timeStep - 1;
            if (prev_id == -1)
            {
                break;
            }
        }
    }
    return path;
}

PathNode::PathNode(Node* node, int timeStep, int prev_id)
{
    this->node = node;
    this->timeStep = timeStep;
    this->prev_id = prev_id;
}
