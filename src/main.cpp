#include "graph.h"
#include "cbs.h"
#include "lowlevel.h"

int final_main()
{
    vector<Node*> nodes;

    Node a(0,0,0);
    nodes.push_back(&a);
    Node b(1,0,0);
    nodes.push_back(&b);
    Node c(2,0,0);
    nodes.push_back(&c);
    Node d(3,0,0);
    nodes.push_back(&d);
    Node e(4,0,0);
    nodes.push_back(&e);
    Node f(2,1,0);
    nodes.push_back(&f);

    vector<Edge> edges;

    for (int i=0; i<nodes.size(); i++)
    {
        edges.push_back(Edge(nodes[i], nodes[i]));
    }

    edges.push_back(Edge(nodes[0],nodes[1]));
    edges.push_back(Edge(nodes[1],nodes[2]));
    edges.push_back(Edge(nodes[2],nodes[3]));
    edges.push_back(Edge(nodes[3],nodes[4]));
    edges.push_back(Edge(nodes[1],nodes[0]));
    edges.push_back(Edge(nodes[2],nodes[1]));
    edges.push_back(Edge(nodes[3],nodes[2]));
    edges.push_back(Edge(nodes[4],nodes[3]));
    edges.push_back(Edge(nodes[2],nodes[5]));
    edges.push_back(Edge(nodes[5],nodes[2]));
    Graph graph(edges);

    Agent A;
    A.id = 1;
    A.startNode = &a;
    A.goalNode = &e;
    Agent B;
    B.id = 2;
    B.startNode = &b;
    B.goalNode = &d;
    vector<Agent*> agents;
    agents.push_back(&A);
    agents.push_back(&B);

    MAPF mapf;
    mapf.agents = agents;
    mapf.graph = &graph;

    Solution solution = solve_cbs(&mapf);

    for (int i=0;i<solution.size();i++)
    {
        cout<<"-------"<<"Agent "<<solution[i].first->id<<"--------"<<endl;
        for (int j=0;j<solution[i].second.size();j++)
        {
            cout<<*solution[i].second[j];
        }
        cout<<endl;
    }
	return 0;
}

int main()
{
    final_main();
    return 0;
}
