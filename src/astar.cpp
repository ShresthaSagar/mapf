#include "astar.h"
#include "graph.h"
#include "cbs.h"
#include "lowlevel.h"
using namespace std;

float Astar::gValueOpen(Node* node)
{
/// returns -1 if the node is not found, else returns g value of the node
    for (int i=0; i<OPEN.size(); i++)
    {
        if (node == OPEN[i].node)
        {
            return OPEN[i].gValue;
        }
    }
    return -1;
}

float Astar::gValueClosed(Node* node)
{
    for (int i=0; i<CLOSED.size(); i++)
    {
        if (node == CLOSED[i].node)
        {
            return CLOSED[i].gValue;
        }
    }
    return -1;
}

float Astar::G(Node* currentNode)
{
    return currentNode->distance(startNode);
}

float Astar::H(Node* currentNode)
{
    return currentNode->distance(goalNode);
}

float Astar::F(Node* currentNode)
{
    return G(currentNode) + H(currentNode);
}

vector<Node*> Astar::findPath(Node* startNode, Node* goalNode)
{
    this->startNode = startNode;
    this->goalNode = goalNode;

    QElement start;
    vector<Node*> path;
    start.node = startNode;
    start.gValue = 0;

    OPEN.push_back(start);
    for(int j=0;j<5;j++)
    {
        QElement current;
        lowestRankOpen(current);
        removeNode(OPEN, current.node);
        Node* currentNode = current.node;

        if (current.node != goalNode)
        {
            CLOSED.push_back(current);
            vector<Pair> neighbours = graph->returnNeighbours(*currentNode);

            for (int i=0; i<neighbours.size(); i++)
            {
                bool in_open = false;
                bool in_closed = false;

                float cost = current.gValue + neighbours[i].second;//currentNode->distance(neighbours[i]);
                float gNeighbourOpen = gValueOpen(neighbours[i].first);
                if (gNeighbourOpen >= 0)
                {
                    in_open = true;
                    if (cost < gNeighbourOpen)
                    {
                        removeNode(OPEN, neighbours[i].first);
                        in_open = false;
                    }
                }

                float gNeighbourClosed = gValueClosed(neighbours[i].first);
                if (gNeighbourClosed >=0)
                {
                    in_closed = true;
                    if (cost < gNeighbourClosed)
                    {
                        removeNode(CLOSED, neighbours[i].first);
                        in_closed = false;
                    }
                }

                if ((!in_closed) && (!in_open))
                {
                    QElement temporary;
                    QElement* temporaryPointer = &temporary;

                    Node* currentNodeValue = current.node;
                    temporary.node = neighbours[i].first;

                    currentNodeValue = current.node;

                    temporary.gValue = cost;
                    temporary.fValue = temporary.gValue + H(temporary.node);
                    OPEN.push_back(temporary);

                    neighbours[i].first->parentNode = currentNode;
                }
            }
        }
        else
        {
            break;
        }
    }
    path = reconstructPath();
    return path;

}

void Astar::removeNode(vector<QElement> &container, Node* node)
{
    for (int i=0; i<container.size(); i++)
    {
        if (node == container[i].node)
        {
            vector<QElement>::iterator itr = container.begin() + i;
            container.erase(itr);
            break;
        }
    }
}


void Astar::lowestRankOpen(QElement &best_element)
{
    best_element = OPEN[0];
    for (int i=1; i<OPEN.size(); i++)
    {
        if (OPEN[i].fValue < best_element.fValue)
        {
            best_element = OPEN[i];
        }
    }
}

vector<Node*> Astar::reconstructPath()
{
    vector<Node*> path;
    path.push_back(goalNode);
    Node* currentNode = goalNode;
    while (currentNode != startNode)
    {
        path.push_back(currentNode->parentNode);
        currentNode = currentNode->parentNode;
    }
    return path;
}
