#include "graph.h"
using namespace std;

int Node::counter = 0;

Node::Node(float x, float y, float theta){
    id = counter;
    counter++;
    this->x = x;
    this->y = y;
    this->theta = theta;
}

float Node::distance(Node* node)
{
    return sqrt(pow(this->x - node->x, 2) + pow(this->y - node->y, 2));
}

ostream & operator << (ostream &out, const Node &n){
    out << "("<< n.id << ": " << n.x << ", "<< n.y << ", " << n.theta << ")";
}

Edge::Edge(Node* src, Node* dest){
    this->src = src;
    this->dest = dest;
    if (src == dest)
    {
        this->distance = 0.5;
    }
    else
    {
        this->distance = sqrt(pow(src->x - dest->x, 2) + pow(src->y - dest->y, 2));
    }
}

ostream & operator << (ostream &out, const Edge &e){
    out << "("<< e.src->id << ", " << e.dest->id << ", "<< e.distance << ")";
}

// Graph Constructor
Graph::Graph(vector<Edge> const &edges)
{
    // resize the vector to N elements of type vector<Pair>
    N_edges = edges.size();

    adjList.resize(N_edges);

    // add edges to the directed graph
    for(int i=0;i<N_edges;i++)
    //for (struct Edge &edge: edges)
    {
        Edge edge = edges[i];
        int src = edge.src->id;
        Node* dest = edge.dest;
        float distance = edge.distance;

        // insert at the end
        adjList[src].push_back(make_pair(dest, distance));

        // Uncomment below line for undirected graph
        // adjList[dest].push_back(make_pair(src, weight));
    }
}

vector<Pair> Graph::returnNeighbours(Node &node)
{
    vector<Pair> neighbours;
    for(int i=0; i < this->adjList[node.id].size(); i++)
    {
        neighbours.push_back(adjList[node.id][i]);
    }
    return neighbours;
}

// print adjacency list representation of graph
void Graph::print()
{
	for (int i = 0; i < N_edges; i++)
	{
		// print all neighboring vertices of given vertex
		for(int j=0;j<adjList[i].size();j++)
			cout << "(" << i << ", " << adjList[i][j].first->id <<
					", " << adjList[i][j].second << ") ";
		cout << endl;
	}
}
