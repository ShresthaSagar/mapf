#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

/**
 * Class for creating node objects
*/
class Node {
    public:
        /// Helper variable for creating id of the node
        static int counter;
        /// Unique identifier for the node
        int id;
        /// x-coordinate of the node
        float x;
        /// y-coordinate of the node
        float y;
        /// Orientation of the node (might not be required) - not in use currently
        float theta;
        /// For low level search algorithm. Link to the parent Node
        Node* parentNode;
        /// For low level search algorithm
        int timeStep;

        /// Constructer Node
        Node(float x, float y, float theta);

        /// Compute distance from this node to the given node in argument
        float distance(Node* node);

        /// For printing node
        friend ostream & operator << (ostream &out, const Node &n);
};

/**
* Data Struture to store edges
*/
class Edge {
    public:
        /// Pointer to src node
        Node* src;
        /// Pointer to destination node
        Node* dest;
        /// Length of the edge
        float distance;

        /// Edge constructor
        /// Also sets distance
        Edge(Node* src, Node* dest);

        /// Helper Function to print out the edge
        friend ostream & operator << (ostream &out, const Edge &e);
};

/// Node and distance from the node (Edge length) to the current node
typedef pair<Node*, float> Pair;

/// class to represent a graph object
class Graph
{
public:
    int N_edges;
	/// construct a vector of vectors of Pairs to represent an adjacency list
	vector<vector<Pair> > adjList;

	/// Graph Constructor
	Graph(vector<Edge> const &edges);

	/// Returns adjacent nodes of the given node
	vector<Pair> returnNeighbours(Node &node);

	/// print adjacency list representation of graph
	void print();
};

#endif
