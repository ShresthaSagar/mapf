#ifndef LOWLEVEL_H
#define LOWLEVEL_H

#include "astar.h"
#include "cbs.h"

/**
* Node class for lowlevel path computation
*/
class PathNode
{
    public:
        Node* node;
        /// Time step of the given node.
        /// Unlike regular search a single node can be visited multiple times, hence timestep and node uniquely identify a Pathnode
        int timeStep;
        int prev_id;
        PathNode(Node* node, int timeStep, int prev_id);

};

/**
* Low level search algorithm inherist regular search. In this case Astar
*/
class LowLevel: public Astar
{
    private:
        /// Time step of the goal computed by the algorithm.
        /// It is necessary to check if there is collision after agent has reached the goal and resting on it
        int latestGoalConstraint;

    public:
        /// Compute the path consistent with the given vertex (constraints) and edge constraints (edgeConstraints)
        vector<Node*> findPath(Node* startNode, Node* goalNode, const vector<Constraint> &constraints, const vector<EdgeConstraint> &edgeConstraints);

        /// Check if the given node with its associated timeStep is consistent with the constraints. Return False otherwise
        bool isAllowed(Node* node, const vector<Constraint> &constraints);

        /// Reconstruct path using the vector of PathNodes and total number of timesteps
        vector<Node*> reconstructPath(vector<PathNode> &, int);

        /// Remove nodes from the neighbours that are inconsistent with the edgeConstraints.
        /// So that these node are not searched or visited
        void filter_nodes(vector<Pair> &neighbours, Node* currentNode, const vector<EdgeConstraint> &edgeConstraints);
};

#endif
