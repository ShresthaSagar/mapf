#ifndef ASTAR_H
#define ASTAR_H

#include "graph.h"
#include <vector>
using namespace std;

/**
* Data structure for storing individual element in the queue element during search
*/
class QElement
{
    public:
        Node* node;
        /// distance from start node to current node
        float gValue;
        /// sum of the cost to current node - G and estimated optimistic cost to goal node - H
        float fValue;
};

/**
* Implements Astar algorithm
*/
class Astar
{
    protected:
        /// OPEN queue
        vector<QElement> OPEN;
        //vector<QElement> open;
        /// CLOSED queue
        vector<QElement> CLOSED;
        Node* startNode;
        Node* goalNode;

    public:
        /// Graph to search on
        Graph* graph;

        /// find path from startNode to goalNode and return it
        vector<Node*> findPath(Node* startNode, Node* goalNode);

        /// fetch G value of the node from OPEN queue
        float gValueOpen(Node*);

        /// fetch G value of the node from CLOSED queue
        float gValueClosed(Node* node);

        /// return element with lowest rank from the OPEN queue
        void lowestRankOpen(QElement &element);

        /// return vector of adjacent nodes
        vector<Node*> neighbours(Node* currentNode);

        /// Compute G value of currentNode
        float G(Node* currentNode);

        /// Compute H value of currentNode
        float H(Node* currentNode);

        /// Compute F = G + H value of currentNode
        float F(Node* currentNode);

        /// Remove node from the given queue
        void removeNode(vector<QElement> &container, Node* node);

        /// reconstruct path using the pointer to parent link of goal node
        vector<Node*> reconstructPath();
};

#endif // ASTAR_H
