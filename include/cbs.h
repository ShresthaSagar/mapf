#ifndef CBS_H
#define CBS_H

#include <iostream>
#include <vector>
#include <cmath>
#include "graph.h"
using namespace std;

/**
* Data structure for an agent. consists of its dockstation node
*/
class Agent
{
public:
    int id;
    Node *dockStation;
    int status;
    Node *startNode;
    Node *goalNode;
    Agent();
    Agent(int id, Node *dockStation, int status = 0);
    friend ostream & operator << (ostream &out, const Agent &a);

};

/**
* Vertex Constraint
*/
class Constraint
{
public:
    Agent *agent;
    Node *node;
    int timeStep;
    Constraint();
    Constraint(Agent *agent, Node *node, int timeStep);
    friend ostream & operator << (ostream &out, const Constraint &c);
};

/**
* Edge Constraints
* agent cannot be in srcNode at srcTimeStep and destNode at srcTimeStep + 1
*/
class EdgeConstraint
{
public:
    Agent* agent;
    Node* srcNode;
    Node* destNode;
    int srcTimeStep;
    EdgeConstraint();
    EdgeConstraint(Agent *agent, Node* srcNode, Node* destNode, int srcTimeStep);
    friend ostream & operator << (ostream &out, const EdgeConstraint &e);
};

typedef vector<Node *> Path;
typedef vector<pair<Agent *, Path> > Solution;

/**
* Conflict between agents at node at timeStep
*/
class Conflict
{
public:
    vector<Agent *> agents;
    Node *node;
    int timeStep;
};

/**
* Conflict between srcAgent and destAgent moving from srcNode to destNode and vice-versa respectively
* at srcTimeStep and srcTimeStep+1 respectively
*/
class EdgeConflict
{
public:
    Agent* srcAgent;
    Agent* destAgent;
    Node* srcNode;
    Node* destNode;
    int srcTimeStep;
};

/**
* Constraint Tree Node
* refer to CBS paper for details
*/
class CTNode
{
public:
    vector<Constraint> constraints;
    vector<EdgeConstraint> edgeConstraints;
    Solution solution;
    float cost;

    /// Invokes LowLevel path finder given the vertex and edge constraints
    Path low_level(Agent *agent, Graph *graph, vector <Constraint> &constraints, vector<EdgeConstraint> &edgeConstraints);

    /// Find sum of individual cost of solution
    float find_cost();

    /// Search for and return vertex conflict in the given solution
    Conflict find_conflict();

    /// Search for and return edge conflict in the given solution
    EdgeConflict find_edgeConflict();

    /// Helper function for find_conflict (return repetition of nodes in the given vector)
    pair<Node*, vector<int> > find_repetition(vector<Node*> &nodes);
};

/**
* MAPF data structure
*/
class MAPF
{
public:
    Graph *graph;
    vector<Agent *> agents;
};

/// compute the smallest cost ct node from CTNodes for given node_size
CTNode smallest_cost(vector<CTNode> &CTNodes, int node_size);

/// Solve CBS for given mapf
Solution solve_cbs(MAPF *mapf);

void print_solution(Solution &solution);

/// Helper function to adjust the length of solution
/// such that each path has the same length
void adjust_solution_length(Solution &solution);

#endif
