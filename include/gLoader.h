#ifndef GLOADER_H
#define GLOADER_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <boost/algorithm/string.hpp>

class CSVReader
{
public:
    std::string fileName;
    std::string delimeter;

    CSVReader();
    CSVReader(std::string filename, std::string delim = ",") : fileName(filename), delimeter(delim) {}

    static std::vector<std::pair<float, float> > getData(std::string filename, std::string delim = ",");
};

class YAMLReader
{
public:
    std::string fileName;
    std::string delimeter;
    std::string mapFile;
    std::string nodeFile;
    std::string edgeFile;
    std::string mapFolder;
    float centerX, centerY, res;

    YAMLReader(std::string mapfolder, std::string filename);

    std::vector<std::pair<std::string, std::string> > getData(std::string yamlfile);
};

#endif
